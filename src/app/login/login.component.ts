import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

import { AuthService } from '../auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  signup: boolean = false;

  login: FormGroup;

  constructor(fb: FormBuilder, private auth: AuthService) { 
    this.login = fb.group({
      username : new FormControl('', [ Validators.required ]),
      password : new FormControl('', [ Validators.required ])
    });
  }

  ngOnInit() {
  }

  submit(userInfo) {
    this.auth.signIn(userInfo.username, userInfo.password).subscribe();
  }
}
