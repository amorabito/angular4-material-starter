import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/map';

import { AuthService } from '../auth.service';
import { LoadingService } from '../loading/loading.service';

import { ValidateUserNotTaken } from '../validators/ValidateUserNotTaken.validator';
import { ValidateEmailNotTaken } from '../validators/ValidateEmailNotTaken.validator';
import { PasswordMatchValidator } from '../validators/ValidatePasswordsMatch.validator';

import { JwtAuthenticationRequest } from '../entity/User';

@Component({
  selector: 'app-signup-form',
  templateUrl: './signup-form.component.html',
  styleUrls: ['./signup-form.component.css']
})
export class SignupFormComponent implements OnInit {
  
  newAccount: FormGroup;

  constructor(fb: FormBuilder, private _as: AuthService, private _ls: LoadingService) {
    this.newAccount = fb.group({
      username : new FormControl('', [ Validators.required ], ValidateUserNotTaken.createValidator(_as)),
      email    : new FormControl('', [ Validators.required, Validators.email ], ValidateEmailNotTaken.createValidator(_as)),
      password : new FormControl('', [ Validators.required ]),
      passAgain: new FormControl('', [ Validators.required ])  
    }, {
      validator : PasswordMatchValidator.create('password', 'passAgain')
    });
   }

  ngOnInit() {
  }

  createBtnDisabled(): boolean {
    return this.newAccount.invalid || this.newAccount.status === 'PENDING';
  }

  save(newUser: JwtAuthenticationRequest, valid: boolean) {
    if (!valid) return;

    this._as.signUp(newUser.username, newUser.email, newUser.password)
    .subscribe(val => {
      console.log(val);
    },
    err => console.log(err),
    () => console.log("signedUp!"));
  }

  getErrorMessage(control: FormControl) {
    return  control.hasError('required')    ? 'You must enter a value' :
            control.hasError('email')       ? 'Not a valid email' : 
            control.hasError('nameTaken')   ? 'Username is taken' :
            control.hasError('emailTaken')  ? 'Email is taken' : 
            control.hasError('passMismatch')? 'Passwords do not match!' : '';
  }
}