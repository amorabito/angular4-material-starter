import { Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
// import { ProfileComponent } from './profile/profile.component';
// import { CallbackComponent } from './callback/callback.component';

// import { AuthGuardService as AuthGuard } from './auth/auth-guard.service';

// import { CreateRecipeComponent } from './recipe/create/create-recipe.component';
// import { SearchRecipeComponent } from './recipe/search/search-recipe.component';

// import { CreateIngredientComponent } from './ingredient/create/create-ingredient.component';
// import { SearchIngredientComponent } from './ingredient/search/search-ingredient.component';

// import { CreateFoodComponent } from './food/create-food/create-food.component';
// import { CreateLookupComponent } from './lookup/create-lookup/create-lookup.component';

export const ROUTES: Routes = [
  { path: '', component: HomeComponent },
//   { path: 'profile', component: ProfileComponent, canActivate: [AuthGuard] },
//   { path: 'callback', component: CallbackComponent },

//   { path: 'recipe/create', component: CreateRecipeComponent, canActivate: [AuthGuard] },
//   { path: 'recipe/search', component: SearchRecipeComponent, canActivate: [AuthGuard] },
  
//   { path: 'ingredient/create', component: CreateIngredientComponent, canActivate: [AuthGuard] },
//   { path: 'ingredient/search', component: SearchIngredientComponent, canActivate: [AuthGuard] },

//   { path: 'food/create', component: CreateFoodComponent, canActivate: [AuthGuard] },
//   { path: 'lookup/create', component: CreateLookupComponent, canActivate: [AuthGuard] },
  
  { path: '**', redirectTo: '' }
];
