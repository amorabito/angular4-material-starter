import { Injectable } from '@angular/core';

import { MatDialog, MatDialogRef } from '@angular/material';

import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { LoadingComponent } from './loading.component';

@Injectable()
export class LoadingService {

  showLoading: BehaviorSubject<boolean>;
  
  private loadingDialogRef: MatDialogRef<LoadingComponent>;

  constructor(private dialog: MatDialog) { 
    this.showLoading = new BehaviorSubject<boolean>(false);
    this.showLoading.subscribe(show => this.toggleLoaderDialog(show));
  }

  show() {
    this.showLoading.next(true);
  }

  hide() {
    this.showLoading.next(false);
  }
 
  private toggleLoaderDialog(show: boolean) {
    if (show) {
      this.loadingDialogRef = this.dialog.open(LoadingComponent, {
        panelClass   : 'loadingPanel',
        disableClose : true
      });

      this.loadingDialogRef.afterClosed().subscribe(() => {
        if (this.showLoading.value) {
          this.hide();
        }
      });
    } else if (this.loadingDialogRef && !show) {
      this.loadingDialogRef.close();
    }
  }
}
