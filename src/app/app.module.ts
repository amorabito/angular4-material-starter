import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MatIconModule, MatToolbarModule, MatInputModule, MatFormFieldModule, MatCardModule, MatButtonModule, MatDialogModule,
          MatProgressSpinnerModule } from '@angular/material';

import { AppComponent } from './app.component';

import { ROUTES } from './app.routes';

import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { FooterComponent } from './footer/footer.component';
import { LoginComponent } from './login/login.component';
import { SignupFormComponent } from './signup-form/signup-form.component';

import { AuthService } from './auth.service';
import { LoadingComponent } from './loading/loading.component';
import { LoadingService } from './loading/loading.service';
import { HttpService } from './services/http.service';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    FooterComponent,
    LoginComponent,
    SignupFormComponent,
    LoadingComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    BrowserAnimationsModule,
    FormsModule, ReactiveFormsModule,
    MatIconModule, MatToolbarModule, MatInputModule, MatFormFieldModule, MatCardModule, MatButtonModule, MatDialogModule,
    MatProgressSpinnerModule,
    RouterModule.forRoot(ROUTES)
  ],
  entryComponents: [ LoadingComponent ],
  providers: [ HttpService, AuthService, LoadingService ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
