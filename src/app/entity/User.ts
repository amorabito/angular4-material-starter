

export class JwtAuthenticationRequest {
    username: string;
    email: string;
    password: string;
}