import { AbstractControl } from '@angular/forms';

export class PasswordMatchValidator {
    static create(passwordControlName: string, confirmPassControlName: string) {
        return (c: AbstractControl) => {
            let pass = c.get(passwordControlName).value;
            let passAgain = c.get(confirmPassControlName).value;
            if(pass !== passAgain) {
                c.get(confirmPassControlName).setErrors({ passMismatch : true });
            } else {
                return null;
            }
        };
    }
}