import { AbstractControl } from '@angular/forms';
import { AuthService } from '../auth.service';

export class ValidateEmailNotTaken {
  static createValidator(auth: AuthService) {
    return (control: AbstractControl) => {
      return auth.emailTaken(control.value)
        .map(res => res ? { emailTaken: true } : null);
    };
  }
}