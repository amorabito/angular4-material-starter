import { AbstractControl } from '@angular/forms';
import { AuthService } from '../auth.service';

export class ValidateUserNotTaken {
  static createValidator(auth: AuthService) {
    return (control: AbstractControl) => {
      return auth.usernameTaken(control.value)
        .map(res => res ? { nameTaken: true } : null);
    };
  }
}