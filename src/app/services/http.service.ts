import { Injectable } from '@angular/core';

import {
  Http,
  Headers,
  RequestOptions,
  RequestOptionsArgs,
  Response,
  XHRBackend
} from '@angular/http';

import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { LoadingService } from '../loading/loading.service';

@Injectable()
export class HttpService extends Http {
  
  constructor(backend: XHRBackend, defaultOptions: RequestOptions, private _ls: LoadingService) {
       super(backend, defaultOptions);
   }

  // constructor(private _ls: LoadingService) { }

  post(url: string, body: any, options?: RequestOptionsArgs): Observable<Response> {
    return this.showLoading(super.post(url, body, options));
  }

  put(url: string, body: any, options?: RequestOptionsArgs): Observable<Response> {
    return this.showLoading(super.put(url, body, options));
  }

  get(url: string, options?: RequestOptionsArgs, silent?: boolean): Observable<Response> {
    if (silent) {
      return super.get(url, options);
    } else {
      return this.showLoading(super.get(url, options));
    }
  }
  
  showLoading(observable: Observable<Response>): Observable<Response> {
    this.showLoader();
    
    return observable.do(
        () => this.hideLoader(),
        error => this.hideLoader()
    );  
  }

  private showLoader() {
    this._ls.show();
  }

  private hideLoader() {
    this._ls.hide();
  }
}
